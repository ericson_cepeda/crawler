package DTO;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DTO_Pelicula
{
    private String nombrePelicula;

    private ArrayList<String> actores;

    private int catidadActores;

    private Calendar fechaPelicula;

    private boolean ultimosCincoAnos;

    public DTO_Pelicula( )
    {
        this.actores = new ArrayList<String>( );
        catidadActores = 0;
        fechaPelicula = new GregorianCalendar( 0000, 00, 01 );
        ultimosCincoAnos = false;
    }

    public boolean isUltimosCincoAnos( )
    {
        return ultimosCincoAnos;
    }

    public void setUltimosCincoAnos( boolean ultimosCincoAnos )
    {
        this.ultimosCincoAnos = ultimosCincoAnos;
    }

    public void setFechaPelicula( Calendar fechaPelicula )
    {
        this.fechaPelicula = fechaPelicula;
    }

    public Calendar getFechaPelicula( )
    {
        return fechaPelicula;
    }

    public void setFechaPelicula( int prmdia, int prmmes, int prmano )
    {
        Calendar fechaHoy = Calendar.getInstance( );
        int ano = fechaHoy.get( Calendar.YEAR );
        if( ano - prmano <= 5 )
            setUltimosCincoAnos( true );
        else
            setUltimosCincoAnos( false );
        
        this.fechaPelicula = new GregorianCalendar( prmano, prmmes, prmdia );
    }

    public String getNombrePelicula( )
    {
        return nombrePelicula;
    }

    public void setNombrePelicula( String nombrePelicula )
    {
        this.nombrePelicula = nombrePelicula;
    }

    public DTO_Pelicula( ArrayList<String> actores )
    {
        this.actores = actores;
    }

    public ArrayList<String> getActores( )
    {
        return actores;
    }

    public void setActores( ArrayList<String> actores )
    {
        this.actores = actores;
    }

    public void addActor( String prmActor )
    {
        actores.add( prmActor );
        catidadActores++;
    }

    public int getCatidadActores( )
    {
        return catidadActores;
    }

    public void setCatidadActores( int catidadActores )
    {
        this.catidadActores = catidadActores;
    }

}
