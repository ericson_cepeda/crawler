package DTO;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DTO_Actor
{
	private static final int MILLIS_IN_SECOND = 1000;
    private static final int SECONDS_IN_MINUTE = 60;
    private static final int MINUTES_IN_HOUR = 60;
    private static final int HOURS_IN_DAY = 24;
    private static final int DAYS_IN_YEAR = 365; //I know this value is more like 365.24...
    private static final long MILLISECONDS_IN_YEAR =
            (long)MILLIS_IN_SECOND * SECONDS_IN_MINUTE * MINUTES_IN_HOUR
            * HOURS_IN_DAY * DAYS_IN_YEAR;
    public static final String MASCULINO = "Masculino";
    public static final String FEMENINO = "Femenino";
    
    private String nombreActor;

    private boolean tieneMasdeVeinticinco;

    private boolean nacidoFuerdaEEUU;

    private String genero;

    private int edad;

    private int cantidadPeliculas;
    
    private int cantidadFilmo;

    private ArrayList<DTO_Pelicula> peliculas;

    public DTO_Actor( )
    {
        this.nombreActor = "";
        this.peliculas = new ArrayList<DTO_Pelicula>( );
        this.tieneMasdeVeinticinco = false;
        this.nacidoFuerdaEEUU = false;
        this.genero = "";
        this.edad = 0;
        this.cantidadPeliculas = 0;
    }

    public DTO_Actor( String nombreActor, boolean tieneMasdeVeinticinco, boolean nacidoFuerdaEEUU, String genero, int edad, int cantidadPeliculas, ArrayList<DTO_Pelicula> peliculas )
    {
        this.nombreActor = nombreActor;
        this.tieneMasdeVeinticinco = tieneMasdeVeinticinco;
        this.nacidoFuerdaEEUU = nacidoFuerdaEEUU;
        this.genero = genero;
        this.edad = edad;
        this.cantidadPeliculas = cantidadPeliculas;
        this.peliculas = peliculas;
    }
    
    

    public int getCantidadFilmo() {
		return cantidadFilmo;
	}

	public void setCantidadFilmo(int cantidadFilmo) {
		this.cantidadFilmo = cantidadFilmo;
	}

	public void setNacidoFuerdaEEUU(boolean nacidoFuerdaEEUU) {
		this.nacidoFuerdaEEUU = nacidoFuerdaEEUU;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public boolean isTieneMasdeVeinticinco( )
    {
        return tieneMasdeVeinticinco;
    }

    public void setTieneMasdeVeinticinco( boolean tieneMasdeVeinticinco )
    {
        this.tieneMasdeVeinticinco = tieneMasdeVeinticinco;
    }

    public boolean isNacidoFuerdaEEUU( )
    {
        return nacidoFuerdaEEUU;
    }

    public void setNacidoEnEEUU( boolean nacidoFuerdaEEUU )
    {
        this.nacidoFuerdaEEUU = nacidoFuerdaEEUU;
    }

    public String getGenero( )
    {
        return genero;
    }

    public void setGenero( String genero )
    {
        this.genero = genero;
    }

    public int getEdad( )
    {
        return edad;
    }

    public void setEdad( int anoNacimiento, int mesNacimienti,  int diaNacimiento)
    {
    	
    	Calendar hoy = Calendar.getInstance();
    	Calendar fechaNacimiento = new GregorianCalendar( anoNacimiento, mesNacimienti, diaNacimiento );
    	
    	int total = (int) ((hoy.getTimeInMillis() - fechaNacimiento.getTimeInMillis())/MILLISECONDS_IN_YEAR);
    	
        this.edad = total;
        if( edad > 25 )
        {
            setTieneMasdeVeinticinco( true );
        }
    }

    public int getCantidadPeliculas( )
    {
        return cantidadPeliculas;
    }

    public void setCantidadPeliculas( int cantidadPeliculas )
    {
        this.cantidadPeliculas = cantidadPeliculas;
    }

    public String getNombreActor( )
    {
        return nombreActor;
    }
    public void setNombreActor( String nombreActor )
    {
        this.nombreActor = nombreActor;
    }
    public ArrayList<DTO_Pelicula> getPeliculas( )
    {
        return peliculas;
    }
    public void setPeliculas( ArrayList<DTO_Pelicula> peliculas )
    {
        this.peliculas = peliculas;
    }

    public void addPelicula( DTO_Pelicula prmPelicula )
    {
        peliculas.add( prmPelicula );
        cantidadPeliculas++;
    }
}
