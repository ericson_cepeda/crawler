package source;



import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

public class Control {
	
	public String strUrlBase = "";
	
	public static void maint()  
	{
		String a = "crawlStorageFolder";
		String b = "5";
		
//        if (args.length != 2) 
//        {
//                System.out.println("Needed parameters: ");
//                System.out.println("\t rootFolder (it will contain intermediate crawl data)");
//                System.out.println("\t numberOfCralwers (number of concurrent threads)");
//                return;
//        }

        /*
         * crawlStorageFolder is a folder where intermediate crawl data is
         * stored.
         */
        String crawlStorageFolder = a;

        /*
         * numberOfCrawlers shows the number of concurrent threads that should
         * be initiated for crawling.
         */
        int numberOfCrawlers = Integer.parseInt(b);

        CrawlConfig config = new CrawlConfig();

        config.setCrawlStorageFolder(crawlStorageFolder);

        /*
         * Be polite: Make sure that we don't send more than 1 request per
         * second (1000 milliseconds between requests).
         */
        config.setPolitenessDelay(1000);

        /*
         * You can set the maximum crawl depth here. The default value is -1 for
         * unlimited depth
         */
        config.setMaxDepthOfCrawling(5);

        /*
         * You can set the maximum number of pages to crawl. The default value
         * is -1 for unlimited number of pages
         */
        config.setMaxPagesToFetch(1000);

        /*
         * Do you need to set a proxy? If so, you can use:
         * config.setProxyHost("proxyserver.example.com");
         * config.setProxyPort(8080);
         * 
         * If your proxy also needs authentication:
         * config.setProxyUsername(username); config.getProxyPassword(password);
         */

        /*
         * This config parameter can be used to set your crawl to be resumable
         * (meaning that you can resume the crawl from a previously
         * interrupted/crashed crawl). Note: if you enable resuming feature and
         * want to start a fresh crawl, you need to delete the contents of
         * rootFolder manually.
         */
        config.setResumableCrawling(false);

        /*
         * Instantiate the controller for this crawl.
         */
        PageFetcher pageFetcher = new PageFetcher(config);
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
        CrawlController controller;
		
        try {
			controller = new CrawlController(config, pageFetcher, robotstxtServer);
		

	        /*
	         * For each crawl, you need to add some seed urls. These are the first
	         * URLs that are fetched and then the crawler starts following links
	         * which are found in these pages
	         */
	
			controller=agregarSeed(controller);
	
	        /*
	         * Start the crawl. This is a blocking operation, meaning that your code
	         * will reach the line after this only when crawling is finished.
	         */
	        //controller.start(MyCrawler.class, numberOfCrawlers);
	        
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static CrawlController agregarSeed(CrawlController controller)
	{
//		Calendar fechaHoy = Calendar.getInstance();
//		int dia = fechaHoy.get(Calendar.DAY_OF_MONTH);
//		int mes = fechaHoy.get(Calendar.MONTH);
//		String strDia ="";
//		String strMes ="";
//		if(dia<=10)
//		{
//			strDia = "0"+dia;
//		}
//		else
//		{
//			strDia = dia+"";
//		}
//		
//		if(mes<=10)
//		{
//			strMes = "0"+mes;
//		}
//		else
//		{
//			strMes = mes+"";
//		}
		
		controller.addSeed("http://www.imdb.com/");

        return controller;
	}
}
