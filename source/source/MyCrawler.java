package source;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;

import org.apache.hadoop.io.Writable;

import DTO.DTO_Actor;
import DTO.DTO_Pelicula;

import com.google.gson.Gson;

public class MyCrawler implements Writable
{
    private DTO_Actor actor;
    private int contadorArchis=0;
    /**
     * You should implement this function to specify whether the given url should be crawled or not (based on your crawling logic).
     */

    public boolean shouldVisit( String url )
    {
        String htmlLlave;
        String htmlHome = "";
        System.out.println(url);
        if( url.equals( "http://www.imdb.com/" ) )
        {
            htmlLlave = obtenerLlaveHome( url );

            htmlHome = getHtml( url, htmlLlave );

        }
        else if( url.contains( "name/" ) )
        {
            htmlHome = getHtmlName( url );   
        }
        else
        {
        	System.out.println("ERROR");
        	return false;
        }

        return debeEntrar( htmlHome, url );

    }
    
    public boolean shouldVisit( String url, DTO_Actor actor, DTO_Pelicula peliculatemp )
    {
    	String html ="";
    	
        if( url.contains( "title/" ) )
        {
        	html = getHtmlName(url);

            Matcher stars = Filtros.F_LLAVE_TITLE_STARS.matcher(html);
            while(stars.find())
            {
            	System.out.println(stars.group(1));
            	peliculatemp.addActor(stars.group(1));
            }
            actor.addPelicula(peliculatemp);
        }
        return true;

    }
    
    public String obtenerCodigo( String htmlCodigo )
    {
    	Matcher codigo = Filtros.F_LLAVE_TITLE_CONST.matcher(htmlCodigo);
    	if(codigo.find())
    	{
    		String rta= codigo.group(1);
    		return rta;
    	}
    	return "";
    }
    
    public String obtenerHtmlCodigo( String url )
    {
    	String content = null;

        String data = "caller_name=" + "p13nsims-title" + "&" + "count=25"+ "&"+ "specs="+"p13nsims:"+ url.split("title/")[1]+ "&"+ "start="+"0";

        URLConnection connection = null;
        try
        {
            String posrUrl = LlamadasPaginas.C_TITLE_JSON;
            connection = new URL( posrUrl ).openConnection( );
            connection.addRequestProperty( "User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)" );
            connection.addRequestProperty( "Referer", url );
            connection.setDoOutput( true );
            connection.setDoInput( true );

            OutputStreamWriter wr = new OutputStreamWriter( connection.getOutputStream( ) );
            wr.write( data );
            wr.flush( );

            BufferedReader rd = new BufferedReader( new InputStreamReader( connection.getInputStream( ) ) );
            String line;
            while( ( line = rd.readLine( ) ) != null )
            {
                content += line;
            }
            wr.close( );
            rd.close( );
        }
        catch( Exception ex )
        {
            ex.printStackTrace( );
        }

        return content;
    }
    
    public String obtenerLlaveTitle( String url )
    {
        return url.split("title/")[1];
    }
    
    public boolean debeEntrar( String html, String url )
    {
        Matcher matcher;
        if( url.equals( "http://www.imdb.com/" ) )
        {
            String urlEntrar = "";

            matcher = Filtros.F_LLAVE_BORN_TODAY.matcher( html );
            urlEntrar += "http://www.imdb.com/name/";

            busquedaRecursiva( matcher, urlEntrar );
        }
        else if( url.contains( "name/" ) )
        {

            revisarActor(html);
        }

        return true;

        // return !Filtros.FILTERS.matcher(href).matches() &&
        // href.startsWith("http://www.imdb.com/");
    }
    
    public void revisarActor(String html)
    {
    	DTO_Actor actor = new DTO_Actor();
    	Matcher name = Filtros.F_LLAVE_NAME.matcher(html);
    	
    	if(name.find())
    	{
    		String nombreAct = name.group(1);
    		actor.setNombreActor(nombreAct);
    	}
    	Matcher born = Filtros.F_LLAVE_BIRTH.matcher(html);
    	
    	if(born.find())
    	{
    		String[] strEdad = born.group(1).split("-");
    		
    		int anotmp=Integer.parseInt(strEdad[0]);
    		int mestmp=Integer.parseInt(strEdad[1]);
    		int diatmp=Integer.parseInt(strEdad[2]);
    			    		
    		actor.setEdad(anotmp,mestmp,diatmp);
    	}
    	Matcher place = Filtros.F_LLAVE_PLACE.matcher(html);
    	if(place.find())
    	{		
    		actor.setNacidoEnEEUU(true);
    	}else
    	{
    		actor.setNacidoEnEEUU(false);
    	}
    	
    	Matcher esActris = Filtros.F_LLAVE_ACTRIS.matcher(html);
    	if (esActris.find()) {
			actor.setGenero(DTO_Actor.FEMENINO);
		}else
		{
			actor.setGenero(DTO_Actor.MASCULINO);
		}
    	
    	Matcher filmo = Filtros.F_LLAVE_FILMOGRAFIA.matcher(html);
    	int contador=0;
    	while(filmo.find())
    	{
    		contador++;
    	}
    	actor.setCantidadFilmo(contador);
    	
    	Matcher knowFor = Filtros.F_LLAVE_KNOW_FOR.matcher(html);
    	String urlEntrar = "http://www.imdb.com/title/";
    	busquedaRecursivaKnowFor(knowFor, urlEntrar, actor);
    	
    }
    
    public void busquedaRecursivaKnowFor( Matcher matcher, String urlEntrar, DTO_Actor actor )
    {
    	 
        while( matcher.find( ) )
        {
        	DTO_Pelicula peliculatemp = new DTO_Pelicula();
        	peliculatemp.setNombrePelicula(matcher.group(2));
        	String strAno = matcher.group(3).replace('(', ' ');
        	strAno = strAno.replace(')', ' ');
        	strAno = strAno.trim();
        	int anoTemp = Integer.parseInt(strAno);
        	peliculatemp.setFechaPelicula(01, 01, anoTemp);
        	System.out.println(matcher.group(0));
        	System.out.println(matcher.group(1));
        	System.out.println(matcher.group(2));
        	System.out.println(matcher.group(3));
        	actor.addPelicula(peliculatemp);

        	shouldVisit( urlEntrar + matcher.group( 1 ),  actor , peliculatemp );
        }
        pasoSerializacion(actor);
    }
    
    public void busquedaRecursiva( Matcher matcher, String urlEntrar )
    {
        while( matcher.find( ) )
        {
            shouldVisit( urlEntrar + matcher.group( 1 ) );
        }
    }

    public boolean revisarPagina( Matcher matcher, String html, String url )
    {

        if( matcher.find( ) )
        {
            // String href = Whref.getURL( ).toLowerCase( );
            // System.out.println("Paginas: ");
            // System.out.println(href);
            // System.out.println(matcher.group( 0 ));
            // System.out.println(matcher.group( 1 ));

            // if( ( Whref.getParentUrl( ) ).equalsIgnoreCase( darUrlDia( ) ) && href.startsWith( "http://www.imdb.com/name/" ) )
            // {
            // return true;
            //
            // }
            // else
            // {
            // if( ( Whref.getParentUrl( ) ).equalsIgnoreCase( darUrlDia( ) ) && href.startsWith( "http://www.imdb.com/title/" ) )
            // {
            //
            // return true;
            //
            // }
            // else
            // {
            // return false;
            // }
            //
            // }
        }
        else
        {
            return false;
        }
        return true;
    }

    private String getHtmlTitle( String url, String prmLlaveHome, String prmcodigo )
    {
        String content = null;

        String data = "caller_name=" + prmLlaveHome + "&" + "info=" + prmLlaveHome + "tconst="+ prmcodigo;

        URLConnection connection = null;
        try
        {
            String posrUrl = LlamadasPaginas.C_PELICULAS;
            connection = new URL( posrUrl ).openConnection( );
            connection.addRequestProperty( "User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)" );
            connection.addRequestProperty( "Referer", url );
            connection.setDoOutput( true );
            connection.setDoInput( true );

            OutputStreamWriter wr = new OutputStreamWriter( connection.getOutputStream( ) );
            wr.write( data );
            wr.flush( );

            BufferedReader rd = new BufferedReader( new InputStreamReader( connection.getInputStream( ) ) );
            String line;
            while( ( line = rd.readLine( ) ) != null )
            {
                content += line;
            }
            wr.close( );
            rd.close( );
        }
        catch( Exception ex )
        {
            ex.printStackTrace( );
        }

        return content;
    }
    
    private String getHtml( String url, String prmLlaveHome )
    {
        String content = null;

        String data = "parent_request_id=" + prmLlaveHome;

        URLConnection connection = null;
        try
        {
            String posrUrl = LlamadasPaginas.C_HOME;
            connection = new URL( posrUrl ).openConnection( );
            connection.addRequestProperty( "User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)" );
            connection.addRequestProperty( "Referer", url );
            connection.setDoOutput( true );
            connection.setDoInput( true );

            OutputStreamWriter wr = new OutputStreamWriter( connection.getOutputStream( ) );
            wr.write( data );
            wr.flush( );

            BufferedReader rd = new BufferedReader( new InputStreamReader( connection.getInputStream( ) ) );
            String line;
            while( ( line = rd.readLine( ) ) != null )
            {
                content += line;
            }
            wr.close( );
            rd.close( );
        }
        catch( Exception ex )
        {
            ex.printStackTrace( );
        }

        return content;
    }

    private String getHtmlName( String url )
    {
    	String content = null;
    	URLConnection connection = null;
    	try {
    	  connection =  new URL(url).openConnection();
    	  connection.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
    	  Scanner scanner = new Scanner(connection.getInputStream());
    	  scanner.useDelimiter("\\Z");
    	  content = scanner.next();
    	}catch ( Exception ex ) {
    	    ex.printStackTrace();
    	}
    	return content;
    }
    
    public String obtenerLlaveHome( String url )
    {
        URLConnection connection = null;
        try
        {
            connection = new URL( url ).openConnection( );
            connection.addRequestProperty( "User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)" );

            BufferedReader rd = new BufferedReader( new InputStreamReader( connection.getInputStream( ) ) );
            String line;
            while( ( line = rd.readLine( ) ) != null )
            {
                Matcher matcher = Filtros.F_LLAVE_HOME.matcher( line );
                if( matcher.find( ) )
                {
                    String fullRegex = matcher.group( 2 );
                    return fullRegex;
                }
            }
        }
        catch( Exception ex )
        {
            ex.printStackTrace( );
        }

        return "";
    }

    public boolean buscarUrl( Matcher matcher, String Whref )
    {
        // System.out.println(matcher.group( 1 ));
        // System.out.println(Whref);
        return true;
    }

    public String darUrlDia( )
    {
        Calendar fechaHoy = Calendar.getInstance( );
        int dia = fechaHoy.get( Calendar.DAY_OF_MONTH );
        int mes = fechaHoy.get( Calendar.MONTH );
        String strDia = "";
        String strMes = "";

        if( dia <= 10 )
        {
            strDia = "0" + dia;
        }
        else
        {
            strDia = dia + "";
        }

        if( mes <= 10 )
        {
            strMes = "0" + mes;
        }
        else
        {
            strMes = mes + "";
        }
        return "http://www.imdb.com/search/name?birth_monthday=" + strMes + "-" + strDia + "&refine=birth_monthday";
    }

    public void pasoSerializacion( DTO_Actor prmActor )
    {
        String strJson;
        Gson gson = new Gson( );
        strJson = gson.toJson( prmActor, DTO_Actor.class );
        System.out.println( strJson );
        contadorArchis++;
        generarArchivo(strJson);
    }
    
    public void generarArchivo( String gson)
    {
    	try{
    		  File archivo = new File("./imputdir/"+contadorArchis+".txt");
    		  // Create file 
    		  FileWriter fstream = new FileWriter(archivo);
    		  
    		  BufferedWriter out = new BufferedWriter(fstream);
    		  out.write(gson);
    		  //Close the output stream
    		  out.close();
    		  System.out.println("archivo "+ contadorArchis);
    		  }catch (Exception e){//Catch exception if any
    		  System.err.println("Error: " + e.getMessage());
    		  }
    }
    public DTO_Actor crearActorGson(String prmgson)
    {
    	Gson gson = new Gson( );
    	DTO_Actor actor = gson.fromJson( prmgson, DTO_Actor.class );
    	return actor;
    }

	@Override
	public void readFields(DataInput arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void write(DataOutput arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}

}
