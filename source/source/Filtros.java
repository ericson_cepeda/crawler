package source;

import java.util.regex.Pattern;

public class Filtros {

	public final static Pattern FILTERS = Pattern.compile(".*(\\.(css|js|bmp|gif|jpe?g"+"|png|tiff?|mid|mp2|mp3|mp4"+"|wav|avi|mov|mpeg|ram|m4v|pdf"+"|rm|smil|wmv|swf|wma|zip|rar|gz))$");
	public final static Pattern F_HOME_DETAILED = Pattern.compile("(<tr class=\"(even detailed|odd detailed)\">)");
	public final static Pattern F_NAME_KNOWFOR = Pattern.compile("(<h2>Known For</h2>)");
	public final static Pattern F_LLAVE_HOME = Pattern.compile("(\\s*xhr[^\\(]+\\(\".+\",\\s*\")(\\w+)(\"\\);)");
	public final static Pattern F_LLAVE_HOME_FIND = Pattern.compile("(x-imdb-parent-id)");
	public final static Pattern F_LLAVE_BORN_TODAY = Pattern.compile( "<div class=\"born-today-img\">[^>]+href=\"/name/(\\w+)", Pattern.MULTILINE );
	public final static Pattern F_LLAVE_BIRTH = Pattern.compile("<time itemprop=\"birthDate\" datetime=\"([0-9-]+)\">", Pattern.MULTILINE);
	public final static Pattern F_LLAVE_PLACE = Pattern.compile("birth_place=[\\w\\%,]+\">[\\w,\\s]+USA</a>", Pattern.MULTILINE);
	public final static Pattern F_LLAVE_ACTRIS = Pattern.compile("<meta name=\"description\" content=\"[\\w\\s]+,\\s*Actress:", Pattern.MULTILINE);
	public final static Pattern F_LLAVE_KNOW_FOR = Pattern.compile("href=\"/title/(\\w+)/\"\\s*itemprop=\"performerIn\"\\s*>([^\\(]+)\\s*([\\(0-9\\)]+)</a>", Pattern.MULTILINE);
	public final static Pattern F_LLAVE_FILMOGRAFIA = Pattern.compile("<a\\s+onclick=\"\\(new Image\\(\\)\\)\\.src='[^']+';\"\\s*href=\"/(title)|(character)/\\w+/\"", Pattern.MULTILINE);
	public final static Pattern F_LLAVE_TITLE = Pattern.compile("<div class=\"rec_item rec_selected\" data-tconst=\"(\\w+)\"", Pattern.MULTILINE);
	public final static Pattern F_LLAVE_TITLE_CONST=Pattern.compile("tconst\":\"(\\w+)\"");
	public final static Pattern F_LLAVE_TITLE_STARS= Pattern.compile("itemprop=\"actors\"\\s*>([\\w\\s]+)<");
	public final static Pattern F_LLAVE_NAME = Pattern.compile("href=\"bio\"\\s*class=\"canwrap\"\\s*>([\\w\\s]+)<");
}
