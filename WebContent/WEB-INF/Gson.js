var a = {
	"nombreActor" : "",
	"tieneMasdeVeinticinco" : true,
	"nacidoFuerdaEEUU" : false,
	"genero" : "Masculino",
	"edad" : 51,
	"cantidadPeliculas" : 8,
	"cantidadFilmo" : 302,
	"peliculas" : [ {
		"nombrePelicula" : "El discurso del rey ",
		"actores" : [ "Colin Firth", "Geoffrey Rush", "Helena Bonham Carter" ],
		"catidadActores" : 3,
		"fechaPelicula" : {
			"year" : 2010,
			"month" : 1,
			"dayOfMonth" : 1,
			"hourOfDay" : 0,
			"minute" : 0,
			"second" : 0
		},
		"ultimosCincoAnos" : true
	}, {
		"nombrePelicula" : "El discurso del rey ",
		"actores" : [ "Colin Firth", "Geoffrey Rush", "Helena Bonham Carter" ],
		"catidadActores" : 3,
		"fechaPelicula" : {
			"year" : 2010,
			"month" : 1,
			"dayOfMonth" : 1,
			"hourOfDay" : 0,
			"minute" : 0,
			"second" : 0
		},
		"ultimosCincoAnos" : true
	}, {
		"nombrePelicula" : "Love Actually ",
		"actores" : [ "Hugh Grant", "Martine McCutcheon", "Liam Neeson" ],
		"catidadActores" : 3,
		"fechaPelicula" : {
			"year" : 2003,
			"month" : 1,
			"dayOfMonth" : 1,
			"hourOfDay" : 0,
			"minute" : 0,
			"second" : 0
		},
		"ultimosCincoAnos" : false
	}, {
		"nombrePelicula" : "Love Actually ",
		"actores" : [ "Hugh Grant", "Martine McCutcheon", "Liam Neeson" ],
		"catidadActores" : 3,
		"fechaPelicula" : {
			"year" : 2003,
			"month" : 1,
			"dayOfMonth" : 1,
			"hourOfDay" : 0,
			"minute" : 0,
			"second" : 0
		},
		"ultimosCincoAnos" : false
	}, {
		"nombrePelicula" : "A Single Man ",
		"actores" : [ "Colin Firth", "Julianne Moore", "Matthew Goode" ],
		"catidadActores" : 3,
		"fechaPelicula" : {
			"year" : 2009,
			"month" : 1,
			"dayOfMonth" : 1,
			"hourOfDay" : 0,
			"minute" : 0,
			"second" : 0
		},
		"ultimosCincoAnos" : true
	}, {
		"nombrePelicula" : "A Single Man ",
		"actores" : [ "Colin Firth", "Julianne Moore", "Matthew Goode" ],
		"catidadActores" : 3,
		"fechaPelicula" : {
			"year" : 2009,
			"month" : 1,
			"dayOfMonth" : 1,
			"hourOfDay" : 0,
			"minute" : 0,
			"second" : 0
		},
		"ultimosCincoAnos" : true
	}, {
		"nombrePelicula" : "Girl with a Pearl Earring ",
		"actores" : [ "Scarlett Johansson", "Colin Firth", "Tom Wilkinson" ],
		"catidadActores" : 3,
		"fechaPelicula" : {
			"year" : 2003,
			"month" : 1,
			"dayOfMonth" : 1,
			"hourOfDay" : 0,
			"minute" : 0,
			"second" : 0
		},
		"ultimosCincoAnos" : false
	}, {
		"nombrePelicula" : "Girl with a Pearl Earring ",
		"actores" : [ "Scarlett Johansson", "Colin Firth", "Tom Wilkinson" ],
		"catidadActores" : 3,
		"fechaPelicula" : {
			"year" : 2003,
			"month" : 1,
			"dayOfMonth" : 1,
			"hourOfDay" : 0,
			"minute" : 0,
			"second" : 0
		},
		"ultimosCincoAnos" : false
	} ]
}
